package com.koddev.chatapp.Fragments;

import com.koddev.chatapp.Notifications.MyResponse;
import com.koddev.chatapp.Notifications.Sender;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface APIService {
    @Headers(
            {
                    "Content-Type:application/json",
                    "Authorization:key=AAAASL_VJuM:APA91bEF__PcCyeHck3dZtJzUXmrXydG3hVE7hEE2nrDAtRSwjsYPVDGTLsLhva1PoHr-y25KKo1zepgzuLVlGXrqsxZE44w2Ur9xlpacZHztCUA5U_iwktpDTMmcAuTOA-biUUDk4gD0gE2CMQ18Thod2QB4J4dzw"
            }
    )

    @POST("fcm/send")
    Call<MyResponse> sendNotification(@Body Sender body);
}
